package com.example.demogoogle

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.googlesignin.GoogleSignInAI
import com.googlesignin.interfaces.GoogleSignCallback


class MainActivity : AppCompatActivity(), GoogleSignCallback {
    private var mGoogleSignInAI: GoogleSignInAI? = null
    private val GOOGLE_LOGIN_REQUEST_CODE = 1001
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeGoogle()
    }

    /*
    *  Initialize Google instance
    */
    private fun initializeGoogle() {
        mGoogleSignInAI = GoogleSignInAI()
        mGoogleSignInAI!!.setActivity(this@MainActivity)
        mGoogleSignInAI!!.setCallback(this)
        mGoogleSignInAI!!.setRequestCode(GOOGLE_LOGIN_REQUEST_CODE,)
        mGoogleSignInAI!!.setUpGoogleClientForGoogleLogin()
    }

    fun doLogin(view: View?) {
        mGoogleSignInAI!!.doSignIn()
    }

    fun doLogout(view: View?) {
        if (mGoogleSignInAI != null) {
            mGoogleSignInAI!!.doSignout()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (GOOGLE_LOGIN_REQUEST_CODE == requestCode) {
            mGoogleSignInAI!!.onActivityResult(data)
        }
    }

    override fun googleSignInSuccessResult(googleSignInAccount: GoogleSignInAccount?) {
        (findViewById(R.id.textView) as TextView).text =
            "Hello " + googleSignInAccount!!.displayName
        Toast.makeText(this, "Hello " + googleSignInAccount.displayName, Toast.LENGTH_LONG).show()
    }

    override fun googleSignInFailureResult(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}